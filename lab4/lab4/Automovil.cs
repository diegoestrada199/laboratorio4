﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Automovil
    {
        int _modelo;
        double _precio;
        string _marca;
        bool _disponible;
        double _tipocambiodolar;

        public Automovil(int modelo, double precio, string marca, bool disponible, double tipocambiodolar)
        {
            _modelo = modelo;
            _precio = precio;
            _marca = marca;
            _disponible = disponible;
            _tipocambiodolar = tipocambiodolar;
        }
        public int modelo
        {
            get
            {
                return _modelo;
            }
            set
            {
                _modelo = value;
            }
        }
        public double precio
        {
            get
            {
                return _precio;
            }
            set
            {
                _precio = value;
            }
        }
        public string marca
        {
            get
            {
                return _marca;
            }
            set
            {
                _marca = value;
            }
        }
        public bool disponible
        {
            get
            {
                return _disponible;
            }
            set
            {
                _disponible = value;
            }
        }
        public double tipocambiodolar
        {
            get
            {
                return _tipocambiodolar;
            }
            set
            {
                _tipocambiodolar = value;
            }
        }
        public string mostrardiponibilidad()
        {
            if (_disponible)
            {
                return "Esta Disponible";
            }
            else
            {
                return "No esta Disponible";
            }
        }
        public string mostrarinformacion()
        {
            string resultado = "";
            resultado = "Marca: " + _marca + ".";
            resultado += "\nModelo: " + _modelo.ToString() + ".";
            resultado += "\nPrecio de Venta: Q" + _precio.ToString() + ".";
            resultado += "\n" + mostrardiponibilidad() + ".";
            resultado += "\nPrecio en dolares: $" + (_precio / _tipocambiodolar).ToString("#.##") + ".";
            return resultado;
        }
    }
}
