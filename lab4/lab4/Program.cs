﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace lab4
{
    class Program
    {
        static void Main(string[] args)
        {
            Automovil bmw = new Automovil(2010, 100000, "", false, 7.75);
            Console.WriteLine("Ingresar Modelo");
            bmw.modelo = Convert.ToInt32(Console.ReadLine());
            Console.WriteLine("Ingresar Precio");
            bmw.precio = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine("Ingresar Marca");
            bmw.marca = Console.ReadLine();
            Console.WriteLine("Ingresar Disponibilidad");
            bmw.disponible = Convert.ToBoolean(Console.ReadLine());
            Console.WriteLine("Ingresar Tipo de Cambio de Dolar");
            bmw.tipocambiodolar = Convert.ToDouble(Console.ReadLine());
            Console.WriteLine(bmw.mostrarinformacion());
            Console.ReadKey();
        }
    }
}
